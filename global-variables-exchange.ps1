﻿[CmdletBinding()]
param(
    [Parameter()][switch]$NoLog = $false,
    [Parameter()][Object[]]$Logs,
    [Parameter()][switch]$CheckOrganizationalUnit
)

if (!$NoLog) {Write-LogMessage -Message "global-variables-exchange.ps1" -Type ScriptStart -LogProfile $Logs -Verbose:$VerbosePreference;}

$VerboseThisScript = $null

<# Exchange-Configuration #>

$ExchangeConnectionUri = @()
$ExchangeConnectionUri += "http://mail01.contoso.com/Powershell"
$ExchangeConnectionUri += "http://mail02.contoso.com/Powershell"

$ExchangeDirectMailbox = @()
$ExchangeDirectMailbox += "contoso.com"

$ExchangeLinkedMailbox = @()
$ExchangeLinkedMailbox += "adatum.net"

$ExchangeRecipients = @{}
$ExchangeRecipients.Add("Support","support@contoso.com")

$ExchangeContoso = @{}
$ExchangeContoso.Add("ConnectionUri", $ExchangeConnectionUri)
$ExchangeContoso.Add("RelayServer", "relay.contoso.com")
$ExchangeContoso.Add("RelayPort", 25)
$ExchangeContoso.Add("RelaySenderAddress", "support@contoso.com");
$ExchangeContoso.Add("MaxUsersPerDB", 50)
$ExchangeContoso.Add("DomainController", "contoso.com")
$ExchangeContoso.Add("Forest", "contoso.com")
$ExchangeContoso.Add("DirectMailBoxDomain", $ExchangeDirectMailbox)
$ExchangeContoso.Add("LinkedMailBoxDomain", $ExchangeLinkedMailbox)
$ExchangeContoso.Add("MailDomains", $ExchangeMailDomains)
$ExchangeContoso.Add("Recipients", $ExchangeRecipients)

$Exchange = @{}
$Exchange.Add("Contoso", $ExchangeContoso)


if (!$NoLog) {Write-LogMessage -Message "Variable `$Exchange gesetzt" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}

Remove-Variable @("ExchangeContoso", "ExchangeConnectionUri", "ExchangeDirectMailbox", "ExchangeLinkedMailbox", "ExchangeRecipients", "ExchangeMailDomains") -ErrorAction SilentlyContinue -WhatIf:$false;

if (!$NoLog) {Write-LogMessage -Message "global-variables-exchange.ps1" -Type ScriptEnd -LogProfile $Logs -Verbose:$VerbosePreference;}