﻿<#

  Läuft bis jetzt noch nicht automatisch ab, muss noch erweitert werden.

#>

New-Item -Path "c:\" -Name "Powershell" -ItemType Directory
New-Item -Path "c:\Powershell" -Name "Modules" -ItemType Directory

$path = $env:Path + ";c:\PSH"
$modulepath = $env:PSModulePath + ";c:\PSH\Modules"

[Environment]::SetEnvironmentVariable("Path", $path, "Machine")
[Environment]::SetEnvironmentVariable("PSModulePath", $modulepath, "Machine")
