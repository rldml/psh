﻿using module PSHLogging;

<#
    ============================================================================
    ================================ DISCLAIMER ================================
    ============================================================================

    This module isn't finished yet and will be further developed in the future.

    So it don't work right now, but should give a first insight of what is planned.

    Greetings, Ronny Kaufmann, 16.11.2019

    ============================================================================
    ============================== INTRODUCTION ================================
    ============================================================================

    This module deliver a base framework to organize jobs written in powershell.
    The idea is, that something (e.g. an ASP.NET Website) can create a job and get
    it's actual state at every time. 

    Additionally it is planned, that this framework can be used as foundation to make
    this jobs fully scalable for the available ressources (CPU-Cores, Machines,...)
    and to make failover-clustering available. 

    Different from Powershell Workflows you can use the complete set of Powershell
    Cmdlets without losing the option to control everything

    ============================================================================
    =============================== DESCRIPTION ================================
    ============================================================================

    There are three main classes provided with this module: PshCompany, PshTask and
    PshFeature.

    A PshCompany represents any organization you would desire to differentiate from
    other organizations. Each organzition can hold an own subset of PshTasks and
    PshFeatures.

    PshTask is a complete environment to define powershell scripts, it's desired
    starttime, a jobdefinition and the logic to assure every PshTask is just run
    once at all.

    A PshFeature is a subset of information that describes a portion of your IT
    infrastructure (e.g. ActiveDirectory domain information.) and can be requested
    from the scripts to gain the needed information to run properly. PshFeatures can
    be global (e.g. Exchange Server information) or specific for one organization
    (e.g. file shares, specific organziational units in a shared ActiveDirectory,
    printers,...)

    A part of coordination data between different processes are stored in a SQL-
    database. For single machines, it may be a localDB 
    (https://docs.microsoft.com/de-de/sql/database-engine/configure-windows/sql-server-express-localdb?view=sql-server-ver15)
    If you want to use more servers than one, it should be a database on a 
    SQL server in your organization.

    The database has to have the name "PSH". You'll need to create the Table jobs
    like you can find the detailed information in pshdb.jobs.sql in the SQL-Folder.

    ============================================================================
    ============================== PLANNED USAGE ===============================
    ============================================================================

    There will be a script, that has to be configured via Windows Planned Tasks to 
    start every minute. This script loads all Organizsations you configured and
    try to start each PshTask with Start-PshTask. 

#>

class PshProcessInfo{

    <#
        This class stores information about the actual process and the name of the computer
    #>

    [System.Int32]$ProcessID;
    [System.DateTime]$ProcessStartTime;
    [System.String]$Computer;

    PshProcessInfo([System.Int32]$pid){
        $tmp = Get-Process -id  $pid;
        $this.ProcessID = $pid;
        $this.ProcessStartTime = Get-Date -Date $tmp.StartTime -MilliSecond 0;
        $this.Computer = $env:COMPUTERNAME;
    }
}

class PshCompany{
    
    <#

        The class PshCompany stores all relevant information about an organization you want do administrate with PSH.
        
        It stores all PSHTasks related to this organization and all Features.

    #>

    #Alte Datentypen, aus Kompatibilitätsgründen

    [System.String]$Name;
    [System.String]$ShortName;
    [System.Boolean]$Visible;
    [System.Boolean]$Enabled;
    [System.Int32]$StdAdId;

    #Neue Datenmitglieder

    [System.Int32]$Version;
    [System.Guid]$Guid;

    [System.Object[]]$Features;
    [System.Object[]]$Tasks;
    [LogModule[]]$DefaultLogProvider;

    [System.Guid[]]$FeaturesOV;
    [System.Guid[]]$TasksOV;

    [System.String]$Path;
    [System.String]$ConfigPath;

    PshCompany([System.String]$Name, [System.String]$ShortName){
        $this.Initiate($Name, $ShortName);
    }

    PshCompany([System.String]$Name, [System.String]$ShortName, [System.Int32]$StdAdId){
        $this.Initiate($Name, $ShortName);
        $this.StdAdId = $StdAdId;
    }

    PshCompany([System.String]$Name, [System.String]$Shortname, [System.IO.DirectoryInfo]$ConfigPath){
        $this.Initiate($Name, $ShortName);
        $this.SetConfigPath($ConfigPath);
    }

    PshCompany([System.String]$Name, [System.String]$ShortName, [System.Int32]$StdAdId, [System.IO.DirectoryInfo]$ConfigPath){
        $this.Initiate($Name, $ShortName);
        $this.StdAdId = $StdAdId;        
        $this.SetConfigPath($ConfigPath);
    }

    PshCompany([System.IO.FileInfo]$ImportFile){
        $this.Import((&$ImportFile));
        $this.SetVersion();
        $this.SetConfigPath($ImportFile.Directory);
    }

    Initiate([System.String]$Name, [System.String]$ShortName){
        $this.Name = $Name;
        $this.ShortName = $ShortName;
        $this.Visible = $True;
        $this.Enabled = $true;
        $this.Guid = [System.Guid]::newGuid();
        $this.SetVersion();
    }

    SetVersion(){
        $this.Version = 1;
    }

    SetConfigPath([System.IO.DirectoryInfo]$Path){
        $this.Path = $Path.FullName;
        $tmpPath = $Path.FullName.TrimEnd("\") + "\" + $this.Guid.ToString();
        if (!(Test-Path -Path $tmpPath)){
            $tmpPathObject = New-Item -Name $this.Guid.toString() -Path $this.Path -ItemType Directory;
        } 
        $this.ConfigPath = $tmpPath;
    }

    [System.String]CreateExportContent(){
        $z = "`$company = @{`n";
        $z += "`t`"Version`"=`"" + $this.Version.ToString() + "`";`n";
        $z += "`t`"Name`"=`"" + $this.Name + "`";`n";
        $z += "`t`"ShortName`"=`""+ $this.ShortName + "`";`n";
        $z += "`t`"StdAdId`"=`"" + $this.StdAdId.ToString() + "`";`n";
        $z += "`t`"Guid`"=`"" + $this.Guid.ToString() + "`";`n";
        if ($this.Enabled){
            $z += "`t`"Enabled`"=`"true`";`n";
        } else {
            $z += "`t`"Enabled`"=`"false`";`n";
        }
        if ($this.Visible){
            $z += "`t`"Visbible`"=`"true`";`n";
        } else {
            $z += "`t`"Visbible`"=`"false`";`n";
        }
        $z += "`n}`n`n`$company";
        return $z;
    }

    Export(){
        if ([System.String]::IsNullOrEmpty($this.Path)){
            $this.SetConfigPath((Get-Item -Path .\));
        }
        $tmp = $this.CreateExportContent();
        $FileName = $this.Path.TrimEnd("\") + "\" + $this.Guid.toString() + "_company.ps1";
        Out-File -InputObject $tmp -FilePath $FileName -Force -Encoding UTF8;
    }

    Import($object){
        switch ($object.Version){
            "1" {
                $this.Guid = [System.Guid]::Parse($object.Guid);
                if ($object.Enabled -eq "True"){
                    $this.Enabled = $true;
                } else {
                    $this.Enabled = $false;
                }
                if ($object.Visible -eq "True"){
                    $this.Visible = $true;
                } else {
                    $this.Visible = $false;
                }
                $this.Name = $object.Name;
                $this.ShortName = $object.ShortName;
                $this.StdAdId = [System.Int32]::Parse($object.StdAdId);
                break;
            }
        }
    }
}

class LMTaskLog : LogModule{

    <#

        The class LMTaskLog enhances the module PSHLogging, to update PSHTask-Jobstate information in a 
        database and in the PSHTask-definition. This enables the opportunity to request the actual PSHJobstate
        through a regular database query.

        Additionally all log lines of the script will be stored in a special log file, which will be archieved 
        after the end of the task.

    #>

    [PshTask]$PSHTask;
    [System.IO.FileInfo]$LogFile;
    [System.Data.SqlClient.SqlCommand]$SqlCommand;

    LMTaskLog([System.String]$Name, [PshTask]$PSHTask){
        $this.Name = $Name;
        $this.PSHTask = $PSHTask;

        $Name = "Job_" + $this.PSHTask.Guid.toString() + ".log";
        $LogPath = Get-Item  -Path  ($this.PSHTask.JobPath.Substring(0,$this.PSHTask.JobPath.LastIndexOf("\")))
        $this.LogFile = New-Item -Path $LogPath.FullName -Name $Name -Force;
        if ($this.PSHTask.GlobalDBID -ne 0 -and $this.PSHTask.GlobalDBID -ne $null){
            $this.SqlCommand = [System.Data.SqlClient.SqlCommand]::new();
            $this.SqlCommand.Connection = $this.PSHTask.GlobalDatabase;
            $this.SqlCommand.CommandText = "UPDATE T_Jobs SET Progress=@Progress WHERE id=@ID;";
            $devnull = $this.SqlCommand.Parameters.Add("@Progress", [System.Data.SqlDbType]::Int);
            $devnull = $this.SqlCommand.Parameters.Add("@ID", [System.Data.SqlDbType]::Int);
            $this.SqlCommand.Parameters["@ID"].Value = $this.PSHTask.GlobalDBID;
        }
    }

    [void]WriteMessage([System.String]$Message, [System.String]$Type, [System.Int32]$Progress){
        if ($Progress -ne 0){
            $this.PSHTask.SetProgress($Progress);
            $this.PshTask.Export();
            if ($this.PSHTask.GlobalDBID -ne 0 -and $this.PSHTask.GlobalDBID -ne $null){
                $this.SqlCommand.Parameters["@Progress"].Value = $this.PshTask.Progress;
                $this.SqlCommand.Connection.Open();
                $this.SqlCommand.ExecuteNonQuery();
                $this.SqlCommand.Connection.Close();
            }
        }
        $Line = (Get-Date -Format "yyyy-MM-dd HH:mm:ss") + " " + $Type + " " + $this.PshTask.Progress.toString() + " " + $Message;
        Out-File -FilePath $this.LogFile.FullName -Encoding utf8 -Append -InputObject $Line;
    }

    [void]FinalizeLog(){
    }
}

class PshTaskParameter{
    
    <#
        This class stores a parameter. The Guid-Property is need to assure, every parameter appears only once 
        in a PshTask.
    #>

    [System.String]$Name;
    [System.String]$Value;
    [System.Guid]$Guid;

    PshTaskParameter([System.String]$Name, [System.String]$Value){
        $this.Name = $Name;
        $this.Value = $Value;
        $this.Guid = [System.Guid]::NewGuid();
    }
}

class PshReturn{
    
    <#
        A PshReturn-Object contains all relevant data to inspect the result of an operation
    #>

    [System.Boolean]$Result;
    [System.String]$Message;

    PshReturn([System.Boolean]$Result){
        $this.Result = $Result;
        $this.Message = "";
    }

    PshReturn([System.Boolean]$Result, [System.String]$Message){
        $this.Result = $Result;
        $this.Message = $Message;
    }
}

class PshJobState{

    <#
        This class stores any information about an actual Jobstate
    #>

    [System.String]$JobState;
    [System.String]$Computer;
    [System.Int32]$ThreadID;
    [System.DateTime]$ThreadTime;

    PshJobState([System.String]$JobState, [System.String]$Computer, [System.Int32]$ThreadID, [System.DateTime]$ThreadTime){
        $this.Jobstate = $JobState;
        $this.Computer = $Computer;
        $this.ThreadID = $ThreadID;
        $this.ThreadTime = $ThreadTime;
    }
}

class PshTask{

    <#

        The class PshTask stores any relevant data to run a script with defined parameters to a given time

    #>

    ########################################################################
    ###                          Properties                              ###
    ########################################################################

    # Unique Guid to differenciate between PSHTasks
    [System.Guid]$Guid;

    # Version of this object
    [System.Int32]$Version;

    # Path to the script that has to be run
    [System.String]$ScriptPath;

    # List of Parameters needed to run the script
    [PshTaskParameter[]]$Parameter;

    # Script will be done only if PSHTask is enabled
    [System.Boolean]$Enabled;

    # actual JobState
    [System.String]$JobState;

    # actual progress of this Task
    [System.Int32]$Progress;

    # Estimated start time for this Task
    [System.DateTime]$StartTime;

    # Additional timestamps
    [System.DateTime]$WhenCreated;
    [System.DateTime]$WhenStarted;
    [System.DateTime]$WhenFinished;

    # Instead of a start time you can set a number of ticks. Every time,
    # the control script (Start-PSHTask) is called, it will reduce this 
    # value by 1. Is this value 0, the script will be started
    [System.Int32]$Ticks;

    # The initiator of this Task
    [System.String]$Initiator;

    # Description-values for the database
    [System.String]$Target;
    [System.String]$Aktion;
    [System.String]$Company;

    # Paths to PSHTask-definition
    [System.String]$JobPath;
    [System.String]$JobArchivePath;

    # Databaseconnection
    [System.Data.SqlClient.SqlConnection]$GlobalDatabase;
    [System.Int32]$GlobalDBID;

    ########################################################################
    ###                           Constructors                           ###
    ########################################################################

    PshTask([System.String]$ScriptPath, [System.DateTime]$StartTime, [PshTaskParameter[]]$Parameter){
        $this.Init($ScriptPath);
        $this.SetStartTime($StartTime);
        $this.SetTicks(-1);
        $this.AddParameter($Parameter);
    }

    PshTask([System.String]$ScriptPath, [System.Int32]$Ticks, [PshTaskParameter[]]$Parameter){
        $this.Init($ScriptPath);
        $this.SetTicks($Ticks);
        $this.AddParameter($Parameter);
    }

    PshTask([System.String]$ScriptPath, [System.DateTime]$StartTime){
        $this.Init($ScriptPath);
        $this.SetStartTime($StartTime);
        $this.SetTicks(-1);
    }

    PshTask([System.String]$ScriptPath, [System.Int32]$Ticks){
        $this.Init($ScriptPath);
        $this.SetTicks($Ticks);
    }

    PshTask([System.IO.FileInfo]$ImportFile){
        $this.Import((&$ImportFile));
        if ($this.JobState -eq "Finished" -or $this.JobPath -eq "Failed"){
            $this.JobArchivePath = $ImportFile.FullName;
            $this.JobPath = "";
        } else {
            $this.JobPath = $ImportFile.FullName;
            $this.JobArchivePath = $ImportFile.FullName.Replace("\Jobs\", "\JobsArchive\");
        }
    }

    ########################################################################
    ###                          Initialization                          ###
    ########################################################################

    # Initialize with default values
    Init([System.String]$ScriptPath){
        $this.SetVersion();
        $this.Guid = [System.Guid]::NewGuid();
        $this.SetScriptPath($ScriptPath);
        $this.SetTaskState('Initialized');
        $this.Enabled = $true;
        $this.Progress = 0;
        $this.WhenCreated = Get-Date;
    }

    # Check Scriptpath
    SetScriptPath([System.String]$ScriptPath){
        if (Test-Path -Path $ScriptPath -PathType Leaf){
            $sp = Get-Item $ScriptPath;
            $this.ScriptPath = $sp.FullName;
        } else {
            throw "File does not exist.";
        }
    }

    # Define Version of this object
    SetVersion(){
        $this.Version = 1;
    }

    # change process state
    SetProgress([System.Int32]$Progress){
        if ($Progress -lt 0){
            $this.Progress = 0;
            return;
        } elseif($Progress -gt 100){
            $this.Progress = 100;
            return;
        } else {
            $this.Progress = $Progress;
        }
    }

    # set ticks
    SetTicks([System.Int32]$Ticks){
        if ($Ticks -ge -1){
            $this.Ticks = $Ticks;
        } 
    }

    # set start time
    SetStartTime([System.DateTime]$StartTime){
        if ($StartTime.Ticks -gt (Get-Date).Ticks){
            $this.StartTime = $StartTime;
        } else {
            throw "Starttime is in past.";
        }
    }

    # Add parameter
    AddParameter([PshTaskParameter[]]$Parameter){
        $o = @();
        foreach ($p in $this.Parameter){
            $o += $p.Guid;
        }
        foreach($p in $Parameter){
            if ($o -notcontains $p.Guid){
                $this.Parameter += $p;
            } else {
                Write-Warning "parameter is already existing. no action done";
            }
        }
    }

    # Set PSHTask-jobstate
    SetTaskState([System.String]$State){
        if ($State -ne 'Initialized' -and 
            $State -ne 'Pending' -and
            $State -ne 'Finished' -and
            $State -ne "Failed" -and 
            $State -ne "NeedAuthorization" -and
            $State -ne "Broken"){
                throw "Not allowed State"
            } else {
                $this.JobState = $State;
                if ($State -eq "Pending"){
                    $this.WhenStarted = Get-Date;
                }
                if ($State -eq "Finished"){
                    $this.WhenFinished = Get-Date;
                }
            }
    }

    ########################################################################
    ###                           Import/Export                          ###
    ########################################################################

    # Create an PSHTask object and fill with imported data
    Import($object){
        switch ($object.Version){
            "1" {
                $this.Version = 1;
                $this.Guid = [System.Guid]::Parse($object.Guid);
                $this.Progress = [System.Int32]::Parse($object.Progress);
                if ($object.StartTime -ne $null){
                    $this.StartTime = Get-Date -Date $object.StartTime;
                }
                if ($object.WhenCreated -ne $null){
                    $this.WhenCreated = Get-Date -Date $object.WhenCreated;
                } else {
                    $this.WhenCreated = Get-Date;
                }
                if ($object.WhenStarted -ne $null){
                    $this.WhenStarted = Get-Date -Date $object.WhenStarted;
                }
                if ($object.WhenFinished -ne $null){
                    $this.WhenFinished = Get-Date -Date $object.WhenFinished;
                }
                $this.ScriptPath = $object.ScriptPath;
                $this.Target = $object.Target;
                $this.Aktion = $object.Aktion;
                $this.Company = $object.Company;
                $this.Parameter = @();
                foreach($p in $object.Parameter){
                    $tmp = New-PshTaskParameter -Name $p.Name -Value $p.Value;
                    $tmp.Guid = [System.Guid]::Parse($p.Guid);
                    $this.Parameter += $tmp;
                }
                if ($object.Enabled -eq "True"){
                    $this.Enabled = $true;
                } else {
                    $this.Enabled = $false;
                }
                $this.SetTaskState($object.JobState);
                if ($object.GlobalDatabase){
                    $this.GlobalDatabase = [System.Data.SqlClient.SqlConnection]::new($object.GlobalDatabase);
                    $QueryResult = $null;
                    $QueryResult = New-PSHDatabaseQuery -Connection $this.GlobalDatabase -Query (([System.Data.SqlClient.SqlCommand]::new("Select ID from T_Jobs where Guid = '"+ $object.Guid +"';")));
                    if ($QueryResult -ne $null){
                        $this.GlobalDBID = $QueryResult.ID;
                    }
                }
                if ($object.Ticks -ne $null){
                    $this.Ticks = [System.Int32]::Parse($object.Ticks);
                }
                $this.Initiator = $object.Initiator;
                break;
            }
        }
    }

    # create export-data
    [System.String]CreateExportContent(){
        $z = "`$job = @{`n";
        $z += "`t`"Version`"=`"" + $this.Version.ToString() + "`";`n";
        $z += "`t`"ScriptPath`"=`"" + $this.ScriptPath + "`";`n";
        $z += "`t`"Progress`"=`""+ $this.Progress.ToString() + "`";`n";
        $z += "`t`"Target`"=`"" + $this.Target + "`";`n";
        $z += "`t`"Company`"=`"" + $this.Company + "`";`n";
        $z += "`t`"Aktion`"=`"" + $this.Aktion + "`";`n";
        if ($this.StartTime -ne $null){
            $z += "`t`"StartTime`"=`"" + $this.StartTime.ToString("yyyy-MM-dd HH:mm:ss") + "`";`n";
        } 
        if ($this.WhenCreated -ne $null){
            $z += "`t`"WhenCreated`"=`"" + $this.WhenCreated.ToString("yyyy-MM-dd HH:mm:ss") + "`";`n";
        }
        if ($this.WhenFinished -ne $null){
            $z += "`t`"WhenCreated`"=`"" + $this.WhenFinished.ToString("yyyy-MM-dd HH:mm:ss") + "`";`n";
        }
        if ($this.WhenStarted -ne $null){
            $z += "`t`"WhenCreated`"=`"" + $this.WhenStarted.ToString("yyyy-MM-dd HH:mm:ss") + "`";`n";
        }
        if ($this.Ticks -ne $null){
            $z += "`t`"Ticks`"=`"" + $this.Ticks.ToString() + "`";`n";
        }
        if ($this.Parameter.Count -gt 0){
            $z += "`t`"Parameter`"=@(`n";
            for ($i = 0; $i -lt $this.Parameter.Count; $i++){
                $z += "`t`t@{`"Name`"=`"" + $this.Parameter[$i].Name + "`"; `"Value`"=`"" + $this.Parameter[$i].Value + "`"; `"Guid`"=`"" + $this.Parameter[$i].Guid.toString() + "`"}";
                if (($i+1) -ne $this.Parameter.Count){
                    $z += ",`n";
                } else {
                    $z += "`n`t);`n";
                }
            }
        }
        $z += "`t`"Guid`"=`"" + $this.Guid.ToString() + "`";`n";
        $z += "`t`"Initiator`"=`"" + $this.Initiator + "`";`n";
        if ($this.Enabled){
            $z += "`t`"Enabled`"=`"true`";`n";
        } else {
            $z += "`t`"Enabled`"=`"false`";`n";
        }
        $z += "`t`"JobState`"=`"" + $this.JobState + "`";`n";
        $z += "`t`"GlobalDatabase`"=`"" + $this.GlobalDatabase.ConnectionString + "`";";
        $z += "`n}`n`n`$job";
        return $z;
    }

    # export object
    Export(){
        $z = $this.CreateExportContent();

        if ($this.JobState -ne "Finished" -and $this.JobState -ne "Failed" -and $this.JobState -ne "Broken"){
            if ([System.String]::IsNullOrEmpty($this.JobPath)){
                throw "JobPath not set"
            }
            Out-File -FilePath $this.JobPath -Encoding UTF8 -Force -InputObject $z;
        } else {
            if ([System.String]::IsNullOrEmpty($this.JobArchivePath)){
                throw "JobArchivePath not set"
            }
            Out-File -FilePath $this.JobArchivePath -Encoding UTF8 -Force -InputObject $z;
            Move-Item -Path $this.JobPath.Replace(".ps1", ".log") -Destination $this.JobArchivePath.Substring(0,$this.JobArchivePath.LastIndexOf("\")) -Force;
            If (Test-Path $this.JobPath){
                try{
                    Remove-Item -Path $this.JobPath -Force;
                } catch {
                    throw "Deletion of PSHTask not successfull";
                }
            }
        }
    }

    # export object
    Export([System.String]$ConfigPath){

        $SavePath = $ConfigPath.TrimEnd("\") + "\";
        if (!(Test-Path -Path $SavePath)){
            $SavePath;
            throw "BasePath not found";
        }

        if (!(Test-Path -Path ($SavePath + "\Jobs"))){
            New-Item -Path $SavePath -Name "Jobs" -ItemType Directory;
        }
        
        if (!(Test-Path -Path ($SavePath + "\JobsArchive"))){
            New-Item -Path $SavePath -Name "JobsArchive" -ItemType Directory;
        }

        $this.JobPath = $SavePath + "\Jobs\Job_" + $this.Guid.ToString() + ".ps1";
        $this.JobArchivePath = $SavePath + "\JobsArchive\Job_" + $this.Guid.ToString() + ".ps1";

        $z = $this.CreateExportContent();

        if ($this.JobState -ne "Finished" -and $this.JobState -ne "Failed" -and $this.JobState -ne "Broken"){
            Out-File -FilePath $this.JobPath -Encoding UTF8 -Force -InputObject $z;
        } else {
            Out-File -FilePath $this.JobArchivePath -Encoding UTF8 -Force -InputObject $z;
            If (Test-Path $this.JobPath){
                try{
                    Remove-Item -Path $this.JobPath -Force;
                } catch {
                    throw "Deletion of PSHTask not successfull";
                }
            }
        }
    }

    ########################################################################
    ###                             Database                             ###
    ########################################################################

    SetGlobalDatabase([System.Data.SqlClient.SqlConnection]$Connection){
        $this.GlobalDatabase = $Connection;
        if ($this.GlobalDBID -eq 0 -or $this.GlobalDBID -eq $null){
            $QueryResult = $null;
            $QueryResult = New-PSHDatabaseQuery -Connection $this.GlobalDatabase -Query (([System.Data.SqlClient.SqlCommand]::new("Select ID from T_Jobs where Guid = '"+ $this.Guid.toString() +"';")));
            if ($QueryResult -ne $null){
                $this.GlobalDBID = $QueryResult.ID;
            }
        }
    }

    # set or update database entry
    UpdateGlobalDB(){
        If ($this.GlobalDatabase -eq $null){
            return;
        }

        if ($this.GlobalDBID -eq 0 -or $this.GlobalDBID -eq $null){
            $QueryResult = $null;
            $QueryResult = New-PSHDatabaseQuery -Connection $this.GlobalDatabase -Query (([System.Data.SqlClient.SqlCommand]::new("Select ID from T_Jobs where Guid = '"+ $this.Guid.toString() +"';")));
            if ($QueryResult -ne $null){
                $this.GlobalDBID = $QueryResult.ID;
            }
        }

        $isNew = $false;
        $SqlCommand = New-Object System.Data.SqlClient.SqlCommand;

        if ($this.GlobalDBID -eq 0 -or $this.GlobalDBID -eq $null){
            $isNew = $true;
            $Statement = "Insert into T_Jobs (Guid,";
            if (![System.String]::IsNullOrEmpty($this.Company)){ $Statement += "Company," };
            $Statement += "Initiator,Target,Action,";
            if ($this.StartTime -ne (Get-Date -Date 0)) {$Statement += "StartTime,"};
            if ($this.Ticks -ne -1){ $Statement += "Ticks,"};
            $Statement += "JobState,Enabled) values (@Guid,";
            if (![System.String]::IsNullOrEmpty($this.Company)) {$Statement += "@Company,"};
            $Statement += "@Initiator,@Target,@Action,";
            if ($this.StartTime -ne (Get-Date -Date 0)) {$Statement += "@StartTime,"};
            if ($this.Ticks -ne -1) {$Statement += "@Ticks,"};
            $Statement += "@JobState,@Enabled)";
            $SqlCommand.Connection = $this.GlobalDatabase;
            $SqlCommand.CommandText = $Statement;
            $devnull = $SqlCommand.Parameters.Add("@Guid", [System.Data.SqlDbType]::NVarChar, 36);
            $SqlCommand.Parameters["@Guid"].Value = $this.Guid.ToString();
            if (![System.String]::IsNullOrEmpty($this.Company)){
                $devnull = $SqlCommand.Parameters.Add("@Company", [System.Data.SqlDbType]::NVarChar, 128);
                $SqlCommand.Parameters["@Company"].Value = $this.Company;
            }
        } else {
            $Statement = "Update T_Jobs set Initiator=@Initiator, Target=@Target, Action=@Action, JobState=@JobState, ";
            if ($this.StartTime -ne (Get-Date -Date 0)){ $Statement += "StartTime=@StartTime, "};
            if ($this.Ticks -ne -1){ $Statement += "Ticks=@Ticks, "};
            $Statement += "Enabled=@Enabled where ID=@ID";
            $devnull = $SqlCommand.Parameters.Add("@ID", [System.Data.SqlDbType]::Int);
            $SqlCommand.Parameters["@ID"].Value = $this.GlobalDBID;
            $SqlCommand.Connection = $this.GlobalDatabase;
            $SqlCommand.CommandText = $Statement;
        }

        $devnull = $SqlCommand.Parameters.Add("@Initiator", [System.Data.SqlDbType]::NVarChar, 128);
        $SqlCommand.Parameters["@Initiator"].Value = $this.Initiator;
        $devnull = $SqlCommand.Parameters.Add("@Target", [System.Data.SqlDbType]::NVarChar, 256);
        $SqlCommand.Parameters["@Target"].Value = $this.Target;
        $devnull = $SqlCommand.Parameters.Add("@Action", [System.Data.SqlDbType]::NVarChar, -1);
        $SqlCommand.Parameters["@Action"].Value = $this.Aktion;
        $devnull = $SqlCommand.Parameters.Add("@JobState", [System.Data.SqlDbType]::NVarChar, 50);
        $SqlCommand.Parameters["@JobState"].Value = $this.JobState;
        
        if ($this.StartTime -ne (Get-Date -Date 0)){
            $devnull = $SqlCommand.Parameters.Add("@StartTime", [System.Data.SqlDbType]::DateTime);
            $SqlCommand.Parameters["@StartTime"].Value = $this.StartTime;
        }

        if ($this.Ticks -ne -1){
            $devnull = $SqlCommand.Parameters.Add("@Ticks", [System.Data.SqlDbType]::Int);
            $SqlCommand.Parameters["@Ticks"].Value = $this.Ticks;
        }

        $devnull = $SqlCommand.Parameters.Add("@Enabled", [System.Data.SqlDbType]::SmallInt);
        if ($this.Enabled){
            $SqlCommand.Parameters["@Enabled"].Value = 1;
        } else {
            $SqlCommand.Parameters["@Enabled"].Value = 0;
        }    

        <# run statement #>
        $SqlCommand.Connection.Open();
        $SqlCommand.ExecuteNonQuery();
        $SqlCommand.Connection.Close();

        # if not done yet: request GlobalID
        if ($isNew){
            $QueryResult = $null;
            $QueryResult = New-PSHDatabaseQuery -Connection $this.GlobalDatabase -Query (([System.Data.SqlClient.SqlCommand]::new("Select ID from T_Jobs where Guid = '"+ $this.Guid.toString() +"';")));
            if ($QueryResult -ne $null){
                $this.GlobalDBID = $QueryResult.ID;
            }
        }
    }

    # get the Jobstate of database
    [PshJobState]GetPshJobStateDB(){
        if ($this.GlobalDBID -eq $null -or $this.GlobalID -eq 0 -or $This.GlobalDatabase -eq $null){
            throw("Keine Datenbank-Verbindung im Job initiiert");
        }
        $Job = New-PshDatabaseQuery -Connection $this.GlobalDatabase -Query (([System.Data.SqlClient.SqlCommand]::new("Select JobState, Computer,ThreadID,ThreadTime from T_Jobs where ID=" + $this.GlobalDBID + ";")));
        if ($Job -eq $null){
            throw ("Job not found in database");
        }
        if ([System.String]::IsNullOrEmpty($Job.ThreadTime)){
            $dt = Get-Date -Date 0;
        } else {
            $dt = Get-Date -Date $Job.ThreadTime;
        }
        if ($Job.ThreadID.GetType().Name -eq "DBNull"){
            $tid = 0;
        } else {
            $tid = [System.Int32]::Parse($Job.ThreadID.toString());
        }
        $erg = New-PshJobState -JobState $Job.JobState -Computer $Job.Computer -ThreadID $tid -ThreadTime $dt;
        return $erg;
    }

    # set the jobstate in database
    SetJobStateDB([System.String]$JobState, [System.String]$Computername, [System.Int32]$ThreadID, [System.DateTime]$ThreadTime){
        $SqlCommand = [System.Data.SqlClient.SqlCommand]::new();
        $Statement = "UPDATE T_Jobs SET JobState=@JobState, Computer=@Computer, ThreadID=@ThreadID, ThreadTime=@ThreadTime where ID = " + $this.GlobalDBID.ToString() + ";";
        $SqlCommand.Connection = $this.GlobalDatabase;
        $SqlCommand.CommandText = $Statement;

        #$devnull = $SqlCommand.Parameters.Add("@EndTime", [System.Data.SqlDbType]::DateTime);
        $devnull = $SqlCommand.Parameters.Add("@JobState", [System.Data.SqlDbType]::NVarChar, 50);
        $devnull = $SqlCommand.Parameters.Add("@Computer", [System.Data.SqlDbType]::NVarChar, 50);
        $devnull = $SqlCommand.Parameters.Add("@ThreadID", [System.Data.SqlDbType]::Int);
        $devnull = $SqlCommand.Parameters.Add("@ThreadTime", [System.Data.SqlDbType]::DateTime);
        
        $SqlCommand.Parameters["@JobState"].Value = $Jobstate;
        $SqlCommand.Parameters["@Computer"].Value = $Computername;
        $SqlCommand.Parameters["@ThreadID"].Value = $ThreadID.toString();
        $SqlCommand.Parameters["@ThreadTime"].Value = $ThreadTime.ToString("yyyy-MM-ddTHH:mm:ss");

        $SqlCommand.Connection.Open();
        $SqlCommand.ExecuteNonQuery();
        $SqlCommand.Connection.Close();
    }

    ########################################################################
    ###                            Execution                             ###
    ########################################################################

    # Checks, if this task is running through another server and/or process and tries to mark this job as his own
    [System.Boolean]CheckAndAssignServer([System.String]$Computer, [System.Int32]$ThreadID, [System.DateTime]$ThreadTime){
        try{
            $Job = $this.GetPshJobStateDB();
        } catch {
            return $true;
        }

        if ($Job.JobState -eq "Initialized" -and [System.String]::IsNullOrEmpty($Job.Computer)){
            $this.SetJobStateDB("Pending", $Computer, $ThreadID, $ThreadTime);
        }

        Start-Sleep -Seconds 3;
        $Job = $this.GetPshJobStateDB();

        if ($Job.JobState -eq "Pending" -and $Job.Computer -eq $Computer -and $Job.ThreadID -eq $ThreadID -and $Job.ThreadTime.Ticks -eq $ThreadTime.Ticks){
            return $true;
        }
        return $false;
    }

    # Check, if this task has to be executed now or have to wait a little longer
    [PshReturn]CheckTask(){
        $Success = $false;
        $Message = "";

        if (!$this.Enabled){return (New-PshReturn -Result $false -Message "Task is deactivated and will be skipped")}
        $now = Get-Date;

        if ($this.StartTime -ne (Get-Date -Date 0)){
            if ($this.StartTime.Ticks -gt $now.Ticks){
                $Message += "Don't reached starttime yet.";
            } else {
                $Success = $true;
            }
        }

        if (!$Success -and $this.Ticks -ne -1){
            if ($this.Ticks -eq 0){
                $Success = $true;
            } else {
                $this.Ticks -= 1;
                $Message += "Decrement ticks by 1";
            }
        }

        $this.UpdateGlobalDB();
        $this.Export();
        
        if ([System.String]::IsNullOrEmpty($Message)){$Message = ""}

        return (New-PshReturn -Result $Success -Message $Message);
    }
}

function New-PshProcessInfo{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.Int32]$pid
    )
    begin{}
    process{
        $erg = [PshProcessInfo]::new($pid);
    }
    end{
        $erg;
    }
    <#

        .SYNOPSIS
        Stores Processinformation

        .DESCRIPTION
        Stores Processinformation in a PSHProcessInfo-Object
    
    #>
}

function New-PSHTaskLog{
    [cmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.String]$Name,
        [Parameter(Mandatory=$true)][PshTask]$PSHTask
    )
    begin{}
    process{
        $erg = [LMTaskLog]::new($Name, $PSHTask);
    }
    end{
        $erg;
    }

    <#

        .SYNOPSIS
        Create a PSHLogging-Object

        .DESCRIPTION
        Create a PSHLogging-Object for a PSHTask

        .PARAMETER Name
        A name for the log

        .PARAMETER PSHTask
        The PSHTask to bind the log

    #>
}

function New-PshTaskParameter{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.String]$Name,
        [Parameter()][System.String]$Value
    )
    begin{}
    process{
        $erg = [PshTaskParameter]::New($Name, $Value);
    }
    end{
        $erg;
    }
    <#

        .SYNOPSIS
        Create a Parameterobject for PSHTask

        .DESCRIPTION
        If the script you want to run with PSHTask needs parameters to do its Jobs, you need to define them with this Cmdlet

        .PARAMETER Name
        Name of the Parameter

        .PARAMETER Value
        Value of the Parameter

    #>
}

function New-PshReturn{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.Boolean]$Result,
        [Parameter()][System.String]$Message
    )
    begin{
        if ([System.String]::IsNullOrEmpty($Message)){
            $Message = "";
        }
    }
    process{
        $erg = [PshReturn]::new($Result, $Message);
    }
    end{
        $erg;
    }
    <#

        .SYNOPSIS
        Create a PSHReturn-Object

        .DESCRIPTION
        A PSHReturn-Object contains a result-value for the information if the script was successfull or not. And a Array of Messages with mroe detailed information.

        .PARAMETER Result
        A value to display the susccess of a script

        .PARAMETER Value
        An array of strings with more detailed informations

    #>
}

function New-PshJobState{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.String]$JobState,
        [Parameter()][System.String]$Computer,
        [Parameter()][System.Int32]$ThreadID,
        [Parameter()][System.DateTime]$ThreadTime
    )
    begin{
        if ([System.String]::IsNullOrEmpty($Computer)){ $Computer = ""; }
        if ($ThreadID -eq $null){ $ThreadID = 0; }
    }
    process{
        $erg = [PshJobState]::new($JobState, $Computer, $ThreadID, $ThreadTime);
    }
    end{
        $erg;
    }
    <#

        .SYNOPSIS
        Create a PSHJobState-Object

        .DESCRIPTION
        This type of object is needed to communicate with the central data store to check the state of a job and to gather information about the parent process and computer

        .PARAMETER JobState
        State of Job

        .PARAMETER Computer
        Name of the host, that runs the script

        .PARAMETER ThreadID
        ID of the parent-process, that runs the script

        .PARAMETER ThreadTime
        Starttime of the parent-process, that runs the script

    #>
}

function Start-PshTask{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$true)][PshTask]$Task,
        [Parameter()][LogProfileData[]]$Logs
    )
    begin{
        if ($logs -eq $null){
            $logs = @(New-PSHTaskLog -Name "TaskLog" -PSHTask $Task);
        } else {
            $logs += New-PSHTaskLog -Name "TaskLog" -PSHTask $Task;
        }
    }
    process{
        $result = $Task.CheckTask();

        if (!$result.Result){
            Write-LogMessage -Message $Result.Message -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
            return;
        }

        $result = $Task.CheckAndAssignServer($env:COMPUTERNAME, $pid, (Get-Date -Date ((Get-Process -Id $pid).StartTime) -MilliSecond 0));

        if (!$result){
            Write-LogMessage -Message "Zuweisung an diesen Thread nicht erfolgt" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
            return;
        }

        $Task.SetTaskState('Pending');
        $Task.Export();

        $zzz = @{};

        $ParameterCount = ($Task.Parameter | Measure-Object).Count;

        Write-LogMessage -Message "Bereite Parameter vor" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
        if ($ParameterCount -gt 0)
        {
            foreach ($par in $Task.Parameter)
            {
                if ([System.String]::IsNullOrEmpty($par.Value)){
                    $zzz.Add($par.Name, $true);
                } else {
                    $zzz.Add($par.Name, $par.Value);
                }
            }
        }
        $zzz.Add("OnlyScriptVerbose", $true);
        if ($log){
            $zzz.Add("Logs", $Logs);
        }
        
        Write-LogMessage -Message ("Lade Script aus Pfad#" + $Task.ScriptPath) -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
        $ScriptFileData = Get-Content -Path $Task.ScriptPath;
            
        $ScriptData = @('param($zzz) &{');
        $ScriptData += $ScriptFileData;
        $ScriptData += '} @zzz';

        $Script = [Scriptblock]::Create($ScriptData);
        $hadCriticalErrors = $false;
        
        Write-LogMessage -Message "F�hre Script aus" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
        try{
            $devnull = Invoke-Command -ScriptBlock $Script -ArgumentList $zzz -ErrorVariable:err;
        } catch {
            $errmsg = @();
            foreach ($e in $err){
                if ($e.GetType().Name -eq "ErrorRecord"){
                    $errmsg += $e;
                }
            }
            $hadCriticalErrors = $true;
        }
        
        $Sessions = Get-PSSession;
        if ($Sessions.Count -gt 0){
            Write-LogMessage -Message "Sessions zu entferntem Host nicht sauber getrennt" -Type Warning -LogProfile $Logs -Verbose:$VerbosePreference;
            $Sessions | Remove-PSSession;
        }

        if ($hadCriticalErrors){
            Write-LogMessage -Message "Kritischer Fehler in der Ausf�hrung des Scripts" -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;
            $Task.SetTaskState('Failed');
            foreach($e in $errmsg){
                Write-LogMessage -Message $e.Exception.Message -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;
            } 
        } elseif(!$hadCriticalErrors -and $err.Count -ne 0){
            Write-LogMessage -Message "Fehler in der Ausf�hrung des Scripts" -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;
            foreach($e in $err){
                Write-LogMessage -Message $e.Exception.Message -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;
            }
            Write-LogMessage -Message "Task ausgef�hrt." -Progress 100 -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
        } else {
            Write-LogMessage -Message "Task ausgef�hrt." -Progress 100 -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
        }

        Publish-LogMessage -LogProfile $Logs;
        if (!$hadCriticalErrors){
            $Task.SetTaskState('Finished');
        }
        $Task.Export();
        $Task.UpdateGlobalDB();
    }
    end{}
}
function New-PshTask{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.String]$ScriptPath,
        [Parameter(Mandatory=$true)][System.String]$Initiator,
        [Parameter(Mandatory=$true)][System.String]$Aktion,
        [Parameter(Mandatory=$true)][System.String]$Target,
        [Parameter()][System.Data.SqlClient.SqlConnection]$GlobalDatabase,
        [Parameter()][PshTaskParameter[]]$Parameter,
        [Parameter()][System.Int32]$Ticks,
        [Parameter()][System.DateTime]$StartTime,
        [Parameter()][System.String]$Company,
        [Parameter()][System.String]$ExportPath
    )
    begin{
        if ($Ticks -eq $null -and $StartTime -eq $null){
            $Ticks = 0;
        }
        if ($ScriptPath -notlike "*.ps1"){
            throw "Kein Powershell-Script!";
        }
    }
    process{
        if ($StartTime -ne $null){
            $erg = [PSHTask]::new($ScriptPath, $StartTime);
            if ($Ticks -eq 0 -or $Ticks -eq -1){
                $erg.SetTicks(-1);
            }
        } else {
            $erg = [PSHTask]::new($ScriptPath, $Ticks);
        }
        if (($Parameter |Measure-Object).Count -ne 0){
            $erg.AddParameter($Parameter);
        }
        $erg.Initiator = $Initiator;
        $erg.Aktion = $Aktion;
        $erg.Target = $Target;
        if (![System.String]::IsNullOrEmpty($Company)){
            $erg.Company = $Company;
        } else {
            $erg.Company = "Global";
        }
        if ($GlobalDatabase -ne $null){
            $erg.SetGlobalDatabase($GlobalDatabase);
            $erg.UpdateGlobalDB();
        }
        if (!(Test-Path -Path $ExportPath)){
            $ExportPath = $ScriptPath.Substring(0,$ScriptPath.LastIndexOf("\"));
        }
        $erg.Export($ExportPath);
    }
    end{
        $erg;
    }
}

function Disable-PshTask{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true),ValueFromPipeline=$true][PshTask[]]$Identity
    )
    begin{}
    process{
        foreach ($i in $Identity){
            $i.Enabled = $false;
            $i.UpdateGlobalDB();
            $i.Export();
        }
    }
    end{}
    <#
        .SYNOPSIS
        Disables a PshTask

        .DESCRIPTION
        Set the Property "Enabled" of a PshTask to "Disabled" ($false)

        .PARAMETER Identity
        One or more PshTask-Objects
    #>
}

function Enable-PshTask{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true),ValueFromPipeline=$true][PshTask[]]$Identity
    )
    begin{}
    process{
        foreach ($i in $Identity){
            $i.Enabled = $true;
            $i.UpdateGlobalDB();
            $i.Export();
        }
    }
    end{}
    <#
        .SYNOPSIS
        Enables a PshTask

        .DESCRIPTION
        Set the Property "Enabled" of a PshTask to "Enabled" ($true)

        .PARAMETER Identity
        One or more PshTask-Objects
    #>
}

function Get-PshTask{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.String]$Path,
        [Parameter()][Switch]$JobsArchive,
        [Parameter()][System.String]$Identity
    )
    begin{
        $Sub = "";
        if (!$JobsArchive){
            $Sub = "\Jobs";
        } else {
            $Sub = "\JobsArchive";
        }
    }
    process{
        $tmp = Get-ChildItem -Path ($Path.TrimEnd("\") + $Sub) -Filter "Job_*.ps1";
        $erg = @();
        foreach ($o in $tmp){
            $erg += [PSHTask]::New($o.FullName);
        }
    }
    end{
        $erg;
    }
}

function New-PSHDatabaseQuery{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.Data.SqlClient.SqlConnection]$Connection,
        [Parameter(Mandatory=$true)][System.Data.SqlClient.SqlCommand]$Query
    )
    begin{}
    process{
        $DS = [System.Data.DataSet]::new();
        $DS.EnforceConstraints = $false;
        $DT = [System.Data.DataTable]::new();
        $DS.Tables.Add($DT);
        $SQL = [System.Data.SqlClient.SqlDataAdapter]::new();
        $Query.CommandType = [System.Data.CommandType]::Text;
        $Query.Connection = $Connection;
        $Query.Connection.Open();
        $SQL.SelectCommand = $Query;
        $SQL.FillSchema($DT, [System.Data.SchemaType]::Source);
        $devnull = $SQL.Fill($DT);
        $Query.Connection.Close();
        $DS.Tables.Remove($DT);
    }
    end{
        $DT;
    }
}

class PshFeature{

    <#
    
        In objects fo this class will special configuration data be stored, that describes the IT environment
        of an organization. 

        An example would be a set of ActiveDirectory domain information, like the FQDN- and Netbios-name, another 
        axample would be Information about Exchange-Servers and where to store specific mailboxes for specific 
        purposes. 

        The class PshFeature is just a baseclass, which need to be inherited to use.

    #>

    [System.Guid]$Guid;
    [System.Int32]$Version;

    [System.String]$FeatureType;

    PshFeature(){
        $type = $this.GetType();
        if ($type -eq "PshFeature"){
            throw "Basisklasse darf nicht instanziiert werden";
        }
    }

}


Export-ModuleMember -Function *-*;