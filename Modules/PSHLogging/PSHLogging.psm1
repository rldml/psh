<#

    ============================================================================
    ============================== INTRODUCTION ================================
    ============================================================================

    This module can be used to write log information to multiple targets within
    your organization without make the source code of the script more complex.

    **NOTE**: This modules works as intended and can already implemented. See 
    unlock-user.ps1 (and also global-variables.ps1) for an example.

    ============================================================================
    =============================== DESCRIPTION ================================
    ============================================================================

    After importing the module in your script, you can define different Log-
    Profiles and store them in one array.

    After that you can use the Write-LogMessage-Cmdlet to write a message. The
    Cmdlet will send the Message to every LogProfile. Some LogProfiles store their
    data instead in an array. To make them write their data to their destination
    you have to use the Publish-LogMessage.

    Per default, you can use five Profiletypes:
      - LMMemory: Stores every message in a array, where you can request it within
            a script.
      - LMVerbose: Write Verbose-Messages
      - LMFile: Write Log messages into files
      - LMEventlog: Gather all LogMessages into an array. Use Publish-LogMessage to
            store a event into a summary
      - LMSQLDB: Write a LogMessage into a data table in a SQL-Database
    
    You can write more LogProfiles to meet your special requirements, but you have to
    inherit from LogModule-Class, and you have to implement a Constructor, A WriteMessage-
    and a FinalizeLog-Method. 

    The advantage of this module is, that you don't need to specifiy a log-target
    while you write a new script. You can just use Write-LogMessage and Publish-LogMessage
    and add the needed LogProvider, before you set your Script in a productive state.

    Additionally, you can differentiate between running a script locally (Verbose) or 
    run them to a planned time and let them write the logs where you can find them later - 
    without making the script more complex.

#>

class LogModule{
    
    <# Base-Class #>

    <# DataStructure #>
    [System.String] $Name;

    LogModule(){
        $type = $this.GetType();
        if ($type -eq [LogModule]){
            throw "Interface Error. May not instantiate base class.";
        }
    }

    WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        throw "Interface Error. Method must be overrided";
    }

    FinalizeLog(){
        throw "Interface Error. Method must be overrided";
    }

}

class LMMemory : LogModule{
    
    <# Write the LogMessage into Memory, where you can request them #>

    [PSObject[]]$logdata;<#Array to store LogMessages#>
    
    LMMemory([System.String]$Name){
        $this.logdata = @();
        $this.Name = $Name;
    }

    [void]WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $MessageObject = New-Object -TypeName PSObject;
        Add-Member -InputObject $MessageObject -NotePropertyName "TimeStamp" -NotePropertyValue (Get-Date -Format "yyyy-MM-ddTHH:mm:ss");
        Add-Member -InputObject $MessageObject -NotePropertyName "Message" -NotePropertyValue $Message;
        Add-Member -InputObject $MessageObject -NotePropertyName "Type" -NotePropertyValue $Type;
        Add-Member -InputObject $MessageObject -NotePropertyName "Progress" -NotePropertyValue $Progress;
        $this.logdata += $MessageObject;
    }

    [void]FinalizeLog(){
        <# do nothing #>
    }
}

class LMVerbose : LogModule{
    
    <# Write Log messages into the Verbose-Channel #>

    LMVerbose([System.String]$Name){
        $this.Name = $Name;
    }

    [void]WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $ts = Get-Date -Format "yyyy-MM-ddTHH:mm:ss";
        $msg = $ts + " " + $Progress.ToString("00") + " " + $Type + " " + $Message;
        Write-Verbose $msg;
    }

    [void]FinalizeLog(){
        <#do nothing#>
    }
}

class LMFile : LogModule{
    
    <# Write Log messages into one or more files #>

    <#Datastructure#>
    [System.IO.DirectoryInfo]$BasePath; <# BasePath to store LogFiles #>
    [System.Int32]$MaxFiles; <# Max Numbers of Files #>
    [System.Int64]$MaxSize; <# Max Size of Logfile in Bytes #>
    [System.Boolean]$FileRotation; <# Enables FileRotation #>

    <# Methods #>

    LMFile([System.String]$Name, [System.String]$BasePath, [System.Int32]$MaxSize){
        $this.Name = $Name;
        $PathExist = Test-Path $BasePath
        if ($PathExist -eq $false){
            throw "Verzeichnispfad nicht gefunden."
        } else {
            $this.BasePath = Get-Item -Path $BasePath;
        }
        $this.MaxSize = $MaxSize;
        $this.DisableFileRotation();
    }

    [void]EnableFileRotation([System.Int32]$MaxFiles){
        $this.FileRotation = $true;
        $this.MaxFiles = $MaxFiles;
    }

    [void]DisableFileRotation(){
        $this.FileRotation = $false;
        $this.MaxFiles = [System.Int32]::MaxValue;
    }

    [System.Int32]GetLogFileNumber([System.IO.FileInfo]$File){
        if ($File.Name -notmatch "\d\d\d\d-\d\d-\d\d_\d\d\d.log"){
            throw "Filename differs from naming pattern."
        }
        $ret = 0
        $Name = $File.Name
        $retString = $Name.Substring($Name.IndexOf("_") + 1, 3)
        $tmp = [System.Int32]::TryParse($retString, [ref]$ret)
        return $ret;
    }

    [System.IO.FileInfo]FindLogFile(){
        $Oldest = $false;
        $FileFilter = (Get-Date -Format "yyyy-MM-dd_*.") + "log";
        $FileList = Get-ChildItem -Path $this.BasePath -Filter $FileFilter;
        if ($FileList.Count -eq 0){
            $ergFile = $this.CreateLogFile((Get-Date -Format "yyyy-MM-dd_") + "001.log");
        } else
        {
            $ergFile = $null;
            foreach($file in $FileList){
                if ($ergFile -ne $null){
                    if ($file.LastWriteTime -gt $ergFile.LastWriteTime){
                        $ergFile = $file;
                    }
                } else {
                    $ergFile = $file;
                }
            }
            if (($ergFile.Length+255) -gt $this.MaxSize){
                $i = $this.GetLogFileNumber($ergFile);
                if ($this.FileRotation){
                    if (($i+1) -gt $this.MaxFiles){
                        $i = 1;
                    } else
                    {
                        $i = $i + 1;
                    }
                } else {
                    #Just add a number and write a new LogFile
                    $i = $i + 1;
                }
                $ergFile = $this.CreateLogFile((Get-Date -Format "yyyy-MM-dd_") + $i.toString("000") + ".log");
            }
        }
        return $ergFile;
    }

    [System.IO.FileInfo] CreateLogFile([System.String]$Name){
        $ergFile = New-Item -Path $this.BasePath.FullName -Name $Name -Force
        return $ergFile;
    }

    [void] WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $file = $this.FindLogFile();
        $Line = (Get-Date -Format "yyyy-MM-dd HH:mm:ss") + " " + $Type + " " + $Message;
        Out-File -FilePath $file.FullName -Encoding utf8 -Append -InputObject $Line
    }

    [void] FinalizeLog(){
        <#do nothing#>
    }
}

class LMEventLog : LogModule{

    <# Write one EventlogEntry at Finalize #>

    [System.String[]] $logdata; <# Array to store LogMessages #>
    [System.String]$EventLogName; <# Name of EventLog #>
    [System.String]$SourceName; <# Name of Source #>
    [System.Int32]$EventID; <# EventID #>
    [System.String] $EntryType; <# Type of Entry. There are three possible Values: Information, Warning and Error #>
    [System.String] $RemoteHost; <# Write Eventlog-Entry  at a different host #>

    LMEventLog([System.String]$Name, [System.String]$LogName, [System.String]$Source, [System.Int32]$EventID){
        $this.logdata = @();
        $this.Name = $Name;
        $this.EventLogName = $LogName;
        $this.SourceName = $Source;
        $this.EventID = $EventID;
        $this.EntryType = "Information";
        $this.RemoteHost = $null;
    }

    [void] SetRemoteHost([System.String]$ComputerName){
        $this.RemoteHost = $ComputerName;
    }

    WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $ts = Get-Date -Format "yyyy-MM-ddTHH:mm:ss";
        $this.logdata += ($ts + " " + $Type + " " + $Message);

        Write-Host ("Before: " + $this.EntryType + ";" + $Type);
        
        if ($this.EntryType -eq "Information" -and ($Type -like "*Warning*" -or $Type -like "*Error*")){
            $this.EntryType = $Type.Trim();
        }

        Write-Host ("After First Check: " + $this.EntryType + ";" + $Type);
        
        if ($this.EntryType -eq "Warning" -and $Type -like "*Error*"){
            $this.EntryType = $Type.Trim();
        }

        Write-Host ("After Second Check: " + $this.EntryType + ";" + $Type);
    }

    FinalizeLog(){
        $MessageString = "";
        foreach($line in $this.logdata){
            $MessageString += $line + "`r`n";
        }
        if ([System.String]::IsNullOrEmpty($this.RemoteHost)){
            Write-EventLog -LogName $this.EventLogName -Source $this.SourceName -EntryType $this.EntryType -Message $MessageString -EventId $this.EventID;
        } else {
            Write-EventLog -LogName $this.EventLogName -Source $this.SourceName -EntryType $this.EntryType -Message $MessageString -EventId $this.EventID -ComputerName $this.RemoteHost;
        }
        $this.logdata = @();
        Write-Host $this.EntryType;
    }
}

class LogProfileDatabaseField{
    [string]$Name
    [System.Data.SqlDbType]$Type
    [int]$Length
}

class LMSQLDB : LogModule{
    
    <# Write Log messages into a database #>

    [System.Data.SqlClient.SqlConnection]$Database;
    [string]$DataBaseTable;
    [LogProfileDatabaseField]$DatabaseMainField;
    [LogProfileDatabaseField[]]$DatabaseFields;
    [System.Char]$MessageIsolator;
    [LogProfileDatabaseField]$LinkIDField;
    [System.Int32]$LinkID;
    [string]$BaseStatement;

    LMSQLDB(
        [System.Data.SqlClient.SqlConnection]$Database, 
        [string]$DataBaseTable,
        [LogProfileDatabaseField]$DatabaseMainField,
        [LogProfileDatabaseField[]]$DatabaseFields,
        [System.Char]$MessageIsolator,
        [LogProfileDatabaseField]$LinkIDField,
        [System.Int32]$LinkID
    ){
        $this.Database = $Database;
        $this.DataBaseTable = $DataBaseTable;
        $this.DatabaseMainField = $DatabaseMainField;
        $this.DatabaseFields = $DatabaseFields;
        $this.MessageIsolator = $MessageIsolator;
        $this.LinkIDField = $LinkIDField;
        $this.LinkID = $LinkID;
        $this.CreateBaseStatement();
    }

    LMSQLDB(
        [System.Data.SqlClient.SqlConnection]$Database, 
        [string]$DataBaseTable,
        [LogProfileDatabaseField]$DatabaseMainField,
        [LogProfileDatabaseField[]]$DatabaseFields,
        [System.Char]$MessageIsolator
    ){
        $this.Database = $Database;
        $this.DataBaseTable = $DataBaseTable;
        $this.DatabaseMainField = $DatabaseMainField;
        $this.DatabaseFields = $DatabaseFields;
        $this.MessageIsolator = $MessageIsolator;
        $this.LinkIDField = $null;
        $this.LinkID = $null;
        $this.CreateBaseStatement();
    }

    CreateBaseStatement(){
        $Statement = "Insert into " + $this.DataBaseTable + " (";
        $Statement += $this.DatabaseMainField.Name + ", ";
        if ($this.LinkIDField -ne $null){
            $Statement += $this.LinkIDField.Name + ", ";
        }
        for ($i = 0; $i -lt $this.DatabaseFields.Count; $i++){
            $Statement += $this.DatabaseFields[$i].Name;
            if (($i+1) -ne $this.DatabaseFields.Count){
                $Statement += ", ";
            } else {
                $Statement += ") values (";
            }
        }
        $Statement += "@" + $this.DatabaseMainField.Name + ", ";
        if ($this.LinkIDField -ne $null){
            $Statement += "@" + $this.LinkIDField.Name + ", ";
        }
        for ($i = 0; $i -lt $this.DatabaseFields.Count; $i++){
            $Statement += "@" + $this.DatabaseFields[$i].Name;
            if (($i+1) -ne $this.DatabaseFields.Count){
                $Statement += ", ";
            } else {
                $Statement += ")";
            }
        }
    }

    WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        <#Datenbank-Insert vorbereiten#>
        $SqlCommand = New-Object System.Data.SqlClient.SqlCommand
        $SqlCommand.Connection = $this.Database
        $SqlCommand.CommandText = $this.BaseStatement

        <#Hauptfeld vorbereitsn#>
        if ($this.DatabaseMainField.Length -ne 0){
            $devnull = $SqlCommand.Parameters.Add("@"+$this.DatabaseMainField.Name, [System.Data.SqlDbType]::NVarChar, $this.DatabaseMainField.Length)
        } else {
            $devnull = $SqlCommand.Parameters.Add("@"+$this.DatabaseMainField.Name, [System.Data.SqlDbType]::NVarChar)
        }

        <#LinkIDField vorbereiten, falls angegeben#>
        if ($this.LinkIDField -ne $null){
            $devnull = $SqlCommand.Parameters.Add("@"+$this.LinkIDField.Name, $this.LinkIDField.Type)
        }

        <#restliche Datenbankfelder vorbereiten#>
        foreach($field in $this.DatabaseFields)
        {
            if ($field.Length -ne 0){
                $devnull = $SqlCommand.Parameters.Add("@"+$field.Name, $field.Type, $field.Length)
            } else {
                $devnull = $SqlCommand.Parameters.Add("@"+$field.Name, $field.Type)
            }
        }

        <#Hauptfeld bef�llen#>
        $SqlCommand.Parameters["@"+$this.DatabaseMainField.Name].Value = $Type

        <#Falls LinkIDField angegeben wurde, bef�llen#>
        if ($this.LinkIDField -ne $null){
            $SqlCommand.Parameters["@"+$this.LinkIDField.Name].Value = $this.LinkID
        }

        <#restliche Felder bef�llen#>
        $messages = $Message.Split($this.MessageIsolator)

        <#Anzahl Nachrichten genauso gro� wie Felderanzahl#>
        if ($messages.Count -eq $this.DatabaseFields.Count){
            for ($i = 0; $i -lt $messages.Count; $i++){
                $SqlCommand.Parameters["@" + $this.DatabaseFields[$i].Name].Value = $messages[$i]
            }
        }

        <#Anzahl Nachrichten gr��er als Felderanzahl#>
        if ($messages.Count -gt $this.DatabaseFields.Count){
            for ($i = 0; $i -lt $this.DatabaseFields.Count - 1; $i++){
                $SqlCommand.Parameters["@" + $this.DatabaseFields[$i].Name].Value = $messages[$i]
            }
            $lastmessage = ""
            for ($i = $this.DatabaseFields.Count - 1; $i -lt $messages.Count; $i++){
                $lastmessage += $messages[$i] + " "
            }
            $SqlCommand.Parameters["@" + $this.DatabaseFields[$this.DatabaseFields.Count - 1].Name].Value = $lastmessage
        }

        <#Anzahl Nachrichten kleiner als Felderanzahl#>
        if ($messages.Count -lt $this.DatabaseFields.Count){
            for ($i = 0; $i -lt $this.DatabaseFields.Count; $i++){
                if ($i -lt $messages.Count){
                    $SqlCommand.Parameters["@" + $this.DatabaseFields[$i].Name].Value = $messages[$i]
                } else
                {
                    $SqlCommand.Parameters["@" + $this.DatabaseFields[$i].Name].Value = ""
                }
            }
        }

        <#Statement ausf�hren#>
        $SqlCommand.Connection.Open()
        $SqlCommand.ExecuteNonQuery()
        $SqlCommand.Connection.Close()
    }

    FinalizeLog(){
        <#do nothing#>
    }
}

function New-LogDatabaseField
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][string]$Name,
        [Parameter(Mandatory=$true)][System.Data.SqlDbType]$Type,
        [Parameter()][int]$Length
    )
    begin{
        if ($Type -eq [System.Data.SqlDbType]::NVarChar -and ($Length -eq 0 -or $Length -eq $null)){
            throw "A varchar-field may not have length 0"
        }
    }
    process{
        $ergObject = New-Object -TypeName LogProfileDatabaseField
        $ergObject.Name = $Name
        $ergObject.Type = $Type
        $ergObject.Length = $Length
    }
    end{
        $ergObject
    }
}

function Write-LogMessage{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)][string]$Message,
        [Parameter(Mandatory=$True)][ValidateSet("Information", "Warning", "Error", "ScriptStart", "ScriptEnd")]$Type,
        [Parameter()]$Progress = 0,
        [Parameter(Mandatory=$True,ValueFromPipeline=$true)][LogModule[]]$LogProfile
    )
    begin{
        switch ($Type){
            "Information" { $TypeOut = "Information" }
            "Warning" { $TypeOut =     "Warning    " }
            "Error" { $TypeOut =       "Error      " }
            "ScriptStart" { $TypeOut = "ScriptStart" }
            "ScriptEnd"{ $TypeOut =    "ScriptEnd  " }
        }
    }
    process{
        foreach($lp in $LogProfile){
            $lp.WriteMessage($Message, $TypeOut, $Progress);
        }
    }
    end{}
}

function Publish-LogMessage{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True,ValueFromPipeline=$true)][LogModule[]]$LogProfile
    )
    begin{}
    process{
        foreach($lp in $LogProfile){
            $lp.FinalizeLog();
        }
    }
    end{}
}


function New-LogProfile{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][ValidateSet("File", "Verbose", "Memory", "EventLog", "Database")][string]$Mode,
        [Parameter(Mandatory=$true)][string]$Name
    )
    DynamicParam {
        if ($Mode -eq "File") {
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            $param = New-Object System.Management.Automation.ParameterAttribute
            $param.Mandatory = $true
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)
        
            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('BasePath', [string], $attributeCollection)
            $paramDictionary.Add('BasePath', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('LogName', [string], $attributeCollection)
            $paramDictionary.Add('LogName', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('MaxSize', [int], $attributeCollection)
            $paramDictionary.Add('MaxSize', $BasePathParam)

            $param.Mandatory = $false
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('FileRotation', [switch], $attributeCollection)
            $paramDictionary.Add('FileRotation', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('MaxFiles', [int], $attributeCollection)
            $paramDictionary.Add('MaxFiles', $BasePathParam)

            return $paramDictionary
        }
        if ($Mode -eq "EventLog") {
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            $param = New-Object System.Management.Automation.ParameterAttribute
            $param.Mandatory = $true
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)
        
            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('LogName', [string], $attributeCollection)
            $paramDictionary.Add('LogName', $ParamEntry)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('Source', [string], $attributeCollection)
            $paramDictionary.Add('Source', $ParamEntry)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('EventID', [int], $attributeCollection)
            $paramDictionary.Add('EventID', $ParamEntry)

            $param.Mandatory = $false
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('ComputerName', [string], $attributeCollection)
            $paramDictionary.Add('ComputerName', $ParamEntry)
            
            return $paramDictionary
        }
        if ($Mode -eq "Database"){
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            $param = New-Object System.Management.Automation.ParameterAttribute
            $param.Mandatory = $true
            $attributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('Database', [System.Data.SqlClient.SqlConnection], $attributeCollection)
            $paramDictionary.Add('Database', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('DataBaseTable', [string], $attributeCollection)
            $paramDictionary.Add('DataBaseTable', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('DatabaseMainField', [LogProfileDatabaseField], $attributeCollection)
            $paramDictionary.Add('DatabaseMainField', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('DatabaseFields', [LogProfileDatabaseField[]], $attributeCollection)
            $paramDictionary.Add('DatabaseFields', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('MessageIsolator', [System.Char], $attributeCollection)
            $paramDictionary.Add('MessageIsolator', $BasePathParam)
            
            $param.Mandatory = $false
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('LinkIDField', [LogProfileDatabaseField], $attributeCollection)
            $paramDictionary.Add('LinkIDField', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('LinkID', [System.Int32], $attributeCollection)
            $paramDictionary.Add('LinkID', $BasePathParam)

            return $paramDictionary
        }
    }
    begin{
        if ($Mode -eq "File"){
            if ($PSBoundParameters.MaxSize -lt 10240){
                throw "MaxSize must be at least 10240 bytes"
            }
            if ($PSBoundParameters.BasePath.ToString().Trim() -eq ""){
                throw "Basepath is mandatory"
            }
            if ($PSBoundParameters.FileRotation -eq $true -and $PSBoundParameters.MaxFiles -lt 1){
                throw "Filerotation needs MaxFiles with a value greater than zero."
            }
        }
        if ($Mode -eq "Database"){
            if ($PSBoundParameters.DatabaseFields.Count -gt 1 -and ($PSBoundParameters.MessageIsolator -eq "" -or $PSBoundParameters.MessageIsolator -eq $null)){
                throw "Wenn mehr als ein Datenfeld angegeben wird, muss auch ein Trenner angegeben werden"
            }

            if ($PSBoundParameters.LinkIDField -ne $null -and $PSBoundParameters.LinkID -eq 0){
                throw "Wenn ein LinkIDField angegeben wird, muss auch eine LinkID �bergeben werden."
            }
        }
    }
    process{
        switch($Mode){
            "File"{
                $BasePath = $PSBoundParameters.BasePath
                $LogName = $PSBoundParameters.LogName
                $FileRotation = $PSBoundParameters.FileRotation
                $MaxSize = $PSBoundParameters.MaxSize
                $MaxFiles = $PSBoundParameters.MaxFiles

                $PathExist = Test-Path $BasePath        
                if (!$PathExist){
                    $tmp = New-Item -Path $BasePath -ItemType Directory
                }
        
                if (!$BasePath.EndsWith("\")){
                    $BasePath = $BasePath + "\"
                }
        
                $UserPath = $BasePath + $LogName
                if (!$UserPath.EndsWith("\")){
                    $UserPath = $UserPath + "\"
                }
                        
                $USerPathExist = Test-Path $UserPath
                if (!$USerPathExist){
                    $tmp = New-Item -Path $UserPath -ItemType Directory
                }
                
                $ergObject = [LMFile]::New($Name, $UserPath, $MaxSize);
                if ($FileRotation){
                    $ergObject.EnableFileRotation($MaxFiles);
                }
            }

            "Verbose"{
                $ergObject = [LMVerbose]::New($Name);
            }

            "Memory"{
                $ergObject = [LMMemory]::New($Name);
            }

            "EventLog"{
                $LogName = $PSBoundParameters.LogName;
                $Source = $PSBoundParameters.Source;
                $EventID = $PSBoundParameters.EventID;
                $RemoteHost = $PSBoundParameters.ComputerName;
                $ergObject = [LMEventLog]::New($Name, $LogName, $Source, $EventID);
                if ($RemoteHost -ne ""){
                    $ergObject.SetRemoteHost($RemoteHost);
                }
            }

            "Database"{
                $Database = $PSBoundParameters.Database;
                $DataBaseTable = $PSBoundParameters.DataBaseTable;
                $DatabaseMainField = $PSBoundParameters.DatabaseMainField;
                $DatabaseFields = $PSBoundParameters.DatabaseFields;
                $MessageIsolator = $PSBoundParameters.MessageIsolator;
                $LinkIDField = $PSBoundParameters.LinkIDField;
                $LinkID = $PSBoundParameters.LinkID;

                if ($LinkIDField -ne $null){
                    $ergObject = [LMSQLDB]::new($Database, $DataBaseTable, $DatabaseMainField, $DatabaseFields, $MessageIsolator, $LinkIDField, $LinkID);
                } else {
                    $ergObject = [LMSQLDB]::new($Database, $DataBaseTable, $DatabaseMainField, $DatabaseFields, $MessageIsolator);
                }
            }
        }
        
    }
    end{
        $ergObject
    }
    <#

        .SYNOPSIS
        Creates a new log profile

        .DESCRIPTION
        Creates a new log profile to send messages to defined targets

        .PARAMETER BasePath
        basepath to create log folders and log files. Only available in File mode

        .PARAMETER LogName
        name of log. Only available in File and Eventlog mode.

        .PARAMETER MaxSize
        maximum size of log files. Only available in File mode.

        .PARAMETER FileRotation
        activates file rotation. only available in File mode

        .PARAMETER MaxFiles
        maximum count of log files. only available in File mode.

        .PARAMETER Mode
        defines the mode of log profile.

        .PARAMETER Source
        the name of the eventsource. Only available in EventLog mode

        .PARAMETER EventID
        The eventid of the event. Only available in EventLog mode

        .PARAMETER ComputerName
        Name of the remote computer. Only available in EventLog mode

        .PARAMETER Name
        name of log profile

    #>
}