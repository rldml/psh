﻿[CmdletBinding()]
param(
    [Parameter()][switch]$NoLog = $false,
    [Parameter()][Object[]]$Logs
)

if (!$NoLog) {Write-LogMessage -Message "global-variables-public.ps1" -Type ScriptStart -LogProfile $Logs -Verbose:$VerbosePreference;}

$VerboseThisScript = $null

# Common Variables

$sysinfo = Get-WmiObject -Class Win32_ComputerSystem

$Umra = @{}
$Umra.Add("Server", @("psh01.contoso.com", "psh02.contoso.com"))
$Umra.Add("ProductiveServer", @("psh01.contoso.com"))
$Umra.Add("DevelopmentServer", @("psh02.contoso.com"))
$Umra.Add("CurrentHost", ($sysinfo.Name + "." + $sysinfo.Domain))
$Umra.Add("DomainController","contoso.com")
$Umra.Add("MainForm","form-menu")
$Umra.Add("GroupPrefix","PSH-")
$Umra.Add("ServicePath", ".\")
$Umra.Add("WorkingPath", "c:\WorkPath\")
$Umra.Add("LogPath", "Logs\");
$Umra.Add("Root", "c:\Powershell\")
if (!$NoLog) {Write-LogMessage -Message "Variable `$Umra gesetzt." -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}

$Current = Get-Date
$Umra.Add("Current", $Current)

$GroupPrefixTable = @{}
$GroupPrefixTable.Add("_Verteiler","VG-");
if (!$NoLog) {Write-LogMessage -Message "Variable `$GroupPrefixTable gesetzt." -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}

$Key = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
$ServiceAccount = @{}
$ServiceAccount.Add("Username","PSHService")
$ServiceAccount.Add("Key", $Key)
$ServiceAccount.Add("Password","76492d1116743f0423413b16050a5345MgB8AEcAWQBYAEgAVAA2AHAAcQBiAGcASQBiAGsAVwA1AGwAbwBmAEUAWABUAEEAPQA9AHwAZgAyADUANgBkAGMAOQBhADgAYQBkAGIAMQA1ADQAOABiAGQAYwA4ADAAYgA1ADkAMAA2AGQANgA5ADkAMQA1ADgANwAwAGUAMQA4AGMAZAA1ADUAYgAzADUAZAAyADEAMABkAGUANwBlADAAOAA5ADYAYQA3ADgAMABiADEAMQA=")
if (!$NoLog) {Write-LogMessage -Message "Variable `$ServiceAccounts gesetzt." -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}

$RegEx = @{};
$RegEx.Add("DateTime","^(((((0?[1-9])|(1\d)|(2[0-8]))\.((0?[1-9])|(1[0-2])))|((31\.((0[13578])|(1[02])))|((29|30)\.((0?[1,3-9])|(1[0-2])))))\.((20[0-9][0-9]))|(29\.0?2\.20(([02468][048])|([13579][26])))) (([0-1]\d)|(2[0-3]))(:|\.)[012345]\d$");
$RegEx.Add("Date", "^(((((0?[1-9])|(1\d)|(2[0-8]))\.((0?[1-9])|(1[0-2])))|((31\.((0[13578])|(1[02])))|((29|30)\.((0?[1,3-9])|(1[0-2])))))\.((20[0-9][0-9]))|(29\.0?2\.20(([02468][048])|([13579][26]))))$");
$RegEx.Add("Time", "^(([0-1]\d)|(2[0-3]))(:|\.)[012345]\d$");
$RegEx.Add("PhoneNumber", "^$|^\+[1-9]\d{1,3}\([1-9]\d{1,4}\)[1-9]\d{1,5}\-?\d{0,6}$");
$RegEx.Add("Number", "^\d*$");
$RegEx.Add("objectSID","^S-\d-\d+-(\d+-){1,14}\d+$");
$RegEx.Add('distinguishedName', '^(?:[A-Za-z][\w-]*|\d+(?:\.\d+)*)=(?:#(?:[\dA-Fa-f]{2})+|(?:[^,=\+<>#;\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*|"(?:[^\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*")(?:\+(?:[A-Za-z][\w-]*|\d+(?:\.\d+)*)=(?:#(?:[\dA-Fa-f]{2})+|(?:[^,=\+<>#;\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*|"(?:[^\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*"))*(?:,(?:[A-Za-z][\w-]*|\d+(?:\.\d+)*)=(?:#(?:[\dA-Fa-f]{2})+|(?:[^,=\+<>#;\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*|"(?:[^\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*")(?:\+(?:[A-Za-z][\w-]*|\d+(?:\.\d+)*)=(?:#(?:[\dA-Fa-f]{2})+|(?:[^,=\+<>#;\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*|"(?:[^\\"]|\\[,=\+<>#;\\"]|\\[\dA-Fa-f]{2})*"))*)*$');
$RegEx.Add('mail',"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
if (!$NoLog) {Write-LogMessage -Message "Variable `$RegEx gesetzt" -Type Information -LogProfile $logs -Verbose:$VerbosePreference;}

Remove-Variable @("Current", "Key", "sysinfo") -ErrorAction SilentlyContinue -WhatIf:$false;

if (!$NoLog) {Write-LogMessage -Message "global-variables-public.ps1" -Type ScriptEnd -LogProfile $Logs -Verbose:$VerbosePreference;}