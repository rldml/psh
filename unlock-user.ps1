﻿[CmdletBinding(SupportsShouldProcess=$True)]
param
(
    [Parameter(Mandatory=$True)][string]$Server,
    [Parameter(Mandatory=$True)][string]$Username,
    [Parameter()][switch]$OnlyScriptVerbose,
    [Parameter()][Object[]]$Logs
)

<#

    This scripts is an example how to use the global-scripts and to implement PshLogging. PshBase isn't fully usable right now, so 
    it is ignored in this script.

#>

<# Executionpolicy has to be unrestricted (actually, but not forever) so it can be called via other PS-Scripts as imported scriptblock #>
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted -Force:$true -Confirm:$false;

. global-init.ps1;

Write-LogMessage -Message "unlock-user.ps1" -Type ScriptStart -LogProfile $Logs -Verbose:$VerbosePreference;

$ErrorActionPreferenceOLD = $ErrorActionPreference;
$ErrorActionPreference = 'stop';

if ($PSCmdlet.ShouldProcess($Username, "Unlock AD-User")){
    Unlock-ADAccount -Identity $Username -Server $Server;
    Set-ADUser -Identity $Username -Server $Server -Replace @{lockoutTime='0'};
    Write-LogMessage -Message "AD-User $Username of $Server has been unlocked" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;
}

$ErrorActionPreference = $ErrorActionPreferenceOLD;

Write-LogMessage -Message "unlock-user.ps1" -Type ScriptEnd -LogProfile $Logs -Verbose:$VerbosePreference;

Publish-LogMessage -LogProfile $Logs;