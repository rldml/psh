﻿if ($Logs.Count -eq 0){
    $LogsIsNull = $true;
} else {
    $LogsIsNull = $false;
}

$VerbosePreferenceOld = $VerbosePreference;
$LogsOld = $Logs;

if ($OnlyScriptVerbose){
    $VerbosePreference = [System.Management.Automation.ActionPreference]::SilentlyContinue;
    Import-Module PshLogging;
    $Logs = @(New-LogProfile -Mode Verbose -Name "Verbose");
}

<#globale Variablen abrufen#>
if (-not $OnlyScriptVerbose -and $LogsIsNull){
    . global-variables.ps1;
}
if ($OnlyScriptVerbose -and $LogsIsNull){
    . global-variables.ps1 -NoLog;
}
if (-not $OnlyScriptVerbose -and -not $LogsIsNull){
    . global-variables.ps1 -Logs $Logs;
}
if ($OnlyScriptVerbose -and -not $LogsIsNull){
    . global-variables.ps1 -NoLog -Logs $Logs;
}

$VerbosePreference = $VerbosePreferenceOld;
if (-not $LogsIsNull){
    $Logs = $LogsOld;
}

$ErrorActionPreferenceOLD = $ErrorActionPreference;
$ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop;