USE [PSH]
GO

/****** Object:  Table [dbo].[T_Jobs]    Script Date: 16.11.2019 16:25:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T_Jobs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Company] [nvarchar](128) NOT NULL,
	[Initiator] [nvarchar](128) NOT NULL,
	[Target] [nvarchar](256) NOT NULL,
	[Action] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Ticks] [int] NULL,
	[Jobstate] [nvarchar](50) NOT NULL,
	[Computer] [nvarchar](50) NULL,
	[ThreadID] [int] NULL,
	[ThreadTime] [datetime] NULL,
	[WhenCreated] [datetime] NULL,
	[WhenStarted] [datetime] NULL,
	[WhenFinished] [datetime] NULL,
	[Enabled] [smallint] NOT NULL,
	[Progress] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[T_Jobs] ADD  CONSTRAINT [DF_T_Jobs_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO

