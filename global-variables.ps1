﻿[CmdletBinding()]
param(
    [Parameter()][Object[]]$Logs,
    [Parameter()][switch]$NoLog = $false
)

Import-Module PshLogging;

if ($Logs.Count -eq 0){
    $Logs = @();
    $Logs += New-LogProfile -Mode Verbose -Name "VerboseLogger";
    $Logs += New-LogProfile -Mode File -BasePath C:\Powershell\Logs -Name "Mainlogger" -MaxSize (20*1024*1024) -FileRotation -MaxFiles 3;
}

if (!$NoLog) {Write-LogMessage -Message "********************************************************************************" -Type Information -LogProfile $Logs;}
if (!$NoLog) {Write-LogMessage -Message "global-variables.ps1" -Type ScriptStart -LogProfile $Logs -Verbose:$VerbosePreference;}

try
{ 
    Import-Module ActiveDirectory -Verbose:$false
    if (!$NoLog) {Write-LogMessage -Message "Modul ActiveDirectory importiert" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}
} catch
{
    if (!$NoLog) {Write-LogMessage -Message "Modul ActiveDirectory nicht importiert" -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;}
}

try
{ 
    Import-Module PshBase -Verbose:$false
    if (!$NoLog) {Write-LogMessage -Message "Modul PshBase importiert" -Type Information -LogProfile $Logs -Verbose:$VerbosePreference;}
} catch
{
    if (!$NoLog) {Write-LogMessage -Message "Modul PshBase nicht importiert" -Type Error -LogProfile $Logs -Verbose:$VerbosePreference;}
}

. ".\global-variables-exchange.ps1" -NoLog:$NoLog -Logs $Logs;
. ".\global-variables-public.ps1" -NoLog:$NoLog -Logs $Logs;

if (!$NoLog) {Write-LogMessage -Message "global-variables.ps1" -Type ScriptEnd -LogProfile $Logs -Verbose:$VerbosePreference;}